from collections import defaultdict, Counter
import subprocess
from nltk import Tree

def collapse_unary(t):
    if type(t) == str:
        return t
    if len(t) == 1 and type(t[0]) == str:
        return t
    if len(t) == 1:
        return collapse_unary(t[0])
    return Tree(t.label(), [collapse_unary(c) for c in t])

def collapse_unaries(input_file, output_file):
    with open(input_file) as f:
        with open(output_file, "w") as o:
            for line in f:
                tree = Tree.fromstring(line.strip())
                if len(tree.leaves()) > 1:
                    tree = collapse_unary(tree)
                stree = " ".join(str(tree).split())
                o.write(f"{stree}\n")

def call_eval(gold_file, pred_file, labelled):
    """Just calls discodop eval and returns Prec, Recall, Fscore as floats"""
    param_file = "proper.prm" if labelled else "proper_unlabelled.prm"

    if not labelled:
        # unlabelled case: ignore unary rewrites
        collapse_unaries(pred_file, f"{pred_file}_no_unary")
        collapse_unaries(gold_file, f"{gold_file}_no_unary")
        params = ["discodop", "eval", f"{gold_file}_no_unary", f"{pred_file}_no_unary", param_file, "--fmt=discbracket", "--verbose", "--disconly"]
    else:
        params = ["discodop", "eval", gold_file, pred_file, param_file, "--fmt=discbracket", "--verbose", "--disconly"]

    print(" ".join(params))
    result = subprocess.check_output(params)

    result = str(result).split("\\n")

    return result

def get_fscore(result):
    recall = result[-6].split()[-1]
    prec = result[-5].split()[-1]
    fscore = result[-4].split()[-1]
    if "nan" not in [prec, recall, fscore]:
        return float(prec), float(recall), float(fscore)
    return 0, 0, 0

def get_per_sentence_information(result):
    result = result[17:]
    keys = result[0].split()
    
    i=2
    sentence_eval = {}
    while result[i].strip() and not result[i].startswith("_________"):
        line = result[i].strip().split()
        #sentence_eval.append(dict(zip(keys, line)))
        sentence_eval[int(line[0])] = dict(zip(keys, line))
        i+=1
    return sentence_eval

def load_annotations(filename):
    annotations = defaultdict(list)
    with open(filename) as f:
        header = f.readline().split(",")
        for line in f:
            line = line.strip().split(",")
            if 'spurious' == line[1]:
                continue
            
            annotations[int(line[0])].append(dict(zip(header, line)))
    return annotations

def per_phenomenon_analysis(sentence_eval, annotations):
    perfect_match = defaultdict(list)
    partial_match = defaultdict(list)
    brackets = defaultdict(list)
    for s_id in annotations:
        notes = annotations[s_id]
        eval_metric = sentence_eval[s_id]
        assert(eval_metric["ID"] == notes[0]["sentence_id"])

        
        recall = float(eval_metric['Recall'])
        precision = float(eval_metric['Precis'])

        phenomenon = "|".join(sorted([n["type"] for n in notes]))

        perfect_match[phenomenon].append(recall == 100 and precision == 100)
        partial_match[phenomenon].append(recall > 0 and precision > 0)

        fscore = [int(v) for v in [eval_metric['Bracket'], eval_metric["gold"], eval_metric["cand"]]]
        brackets[phenomenon].append(fscore)

        #'Recall': '100.00', 'Precis': '100.00', 'Bracket': '2', 'gold': '2', 'cand': '2'

    return perfect_match, partial_match, brackets

def print_statistics_for_annotations(annotations):

    num_sentences = len(annotations)
    phenomena_per_sentence = [len(v) for k, v in annotations.items()]
    num_phenomena_instances = sum(phenomena_per_sentence)
    num_phenomena_per_num = Counter(phenomena_per_sentence)

    print()
    print(f"Number of sentences: {num_sentences}")
    print(f"Number of phenomena occurrences: {num_phenomena_instances}")
    print(f"Number of phenomena per sentence: {num_phenomena_per_num}")
    print()


    more_than_one = [k for k,v in annotations.items() if len(v) > 1]

    print(more_than_one)



def main(args):

    annotations = load_annotations(args.annotations)
    print_statistics_for_annotations(annotations)

    for labelled in [True, False]:
        eval_log = call_eval(args.gold, args.input, labelled)

        fscore, prec, recall = get_fscore(eval_log)
        sentence_eval = get_per_sentence_information(eval_log)

        #    for i in sorted(annotations.items())[:10]:
        #        print(i)
        #    print(len(sentence_eval))
        #    for sent in sorted(sentence_eval.items())[:10]:
        #        print(sent)

        perfect_match, partial_match, brackets_all = per_phenomenon_analysis(sentence_eval, annotations)

        print(f"Labelled = {labelled}")

        print("\t".join(["Phénomène", "Effectif", "Reconnus", "Partiellement reconnus", "Précision", "Rappel", "F$_1$"]))
        for k in sorted(perfect_match, key = lambda x: len(perfect_match[x]), reverse=True):
            ks = k + (" " * (45 - len(k)))
            perfm = perfect_match[k]
            partm = partial_match[k]
            brackets = brackets_all[k]
            n = len(perfm)

            bracket, gold, cand = [sum(l) for l in list(zip(*brackets))]
            recall = bracket / gold
            precision = bracket / cand
            f = 0
            if recall > 0 and precision > 0:
                f = 2 * precision * recall / (precision + recall)

            l = [ks, len(perfm), round(sum(perfm)/n * 100, 1), round(sum(partm)/n*100, 1), round(precision * 100, 1), round(recall * 100, 1), round(f * 100, 1)]
            print("\t".join([f"{v}" for v in l]))
            #print(f"{ks}\t{len(perm)}\t{sum() / len(v):.2f} ")
        print()


if __name__ == "__main__":
    import argparse
    usage = """
        Dependencies: disco-dop, nltk

        python eval.py <gold dev>.discbracket <pred dev>.discbracket annotations_dev_v1.0.csv
    """
    parser = argparse.ArgumentParser(description = usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("gold")
    parser.add_argument("input")
    parser.add_argument("annotations")

    args = parser.parse_args()

    main(args)


